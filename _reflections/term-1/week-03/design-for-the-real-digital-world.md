---
title: Design for the Real Digital World
period: 15 October - 21 October 2018
date: 2018-10-28 18:00:00
term: 1
published: true
---


<iframe src="https://player.vimeo.com/video/297477108" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/297477108">Designing for the Real Digital World</a> from <a href="https://vimeo.com/user91079991">Jessica Guy</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

### Redesign of the MDEF Classroom
**With Ingi Freyr and Francesco Zonca**

It is commonly known that the interior design of a space, has an impact on the productivity and satisfaction of the users. It makes sense that we as designers, should create a pleasing space for the upcoming months, to increase our creativity and ambition. The space given to us by IAAC offers a multitude of possibilities for interaction. Ingi Freyr and Francesco Zonca introduced us to the idea to design our space with **scavenged materials of the street**. This is lowering the cost and the environmental impact of the redesign. Moreover the area in which we live in is reflected through the materials we found and used.

Our personalty has influence on spatial perception, therefor everyone of us has different desires for the space. The emerging interventions are driven by the existing space and found material. We have examined the requirements of our classroom based on our desires and outcomes of these questions.

- How do we move in the space?
- What do we need in the space?
- How can the space be zoned?
- What are the relations between the sections?
- How do we interact in each section?

### Process
Groups where formed by personal skills. Goal was that every group contains at least one 3D modeller, communication designer, documentation person and one person for the manual labor. Our group members where: Adriana Tamargo Iturri, Emily Whyman, Fífa Jónsdóttir, Gabor Mandoki, Katherine Vegas Gracia and myself.

![]({{site.baseurl}}/181026_Redesignroom_needs.jpg)

Following the classical interior design process we first made an inventory of all the materials we have with sizes and photos. Then we wrote down all the needs and desires for the room. Four guideline groups where formed through clustering the needs and desires. The MDEF Plaza concept was taking shape.

![]({{site.baseurl}}/181027_DigitalWorld_Mood.jpg)

We rearranged the floorpan based on our needs and moving behaviour. Each of our group members then sketched designs for every zone of our classroom, to gather as much ideas possible. We used the materials as inspiration, combining shapes, colours an structures to compose something new and interesting.To decide on our final concept we discussed all ideas, refined/ combined them to our final design. Our focus was to use the scavenged materials to a large extent, avoiding new bought materials or wood from the workshop as much as possible. The propose of this was so enhance the charisma of the design.

After deliberating the designs ideas from each group, our group was asked to make a **shelving unit** out of old drawers. As well as a **movable wall**, made of old doors and a **recycling system** for plastic, paper and cans and **bean bags**. Due to the limited time frame we decided to work on the shelving unit and movable wall. The recycling and the bean bags will be realized in the upcoming weeks. It was a pleasure to work in our group. We were able to divide the work between us equally. Everyone could participate in the design process as well in the making process. Through open discussions and honest feedback we were able to work on this project as a real team, without any major problems.

![]({{site.baseurl}}/181028_DigitalWorld_GIF.gif)

### Reflection

We were able to explore the contrast of materials, forming new compositions, finding balance between them. We have learned to use the 3D printer, CNC milling, laser cutting and many other power tools in the workshop of IAAC. It is interesting what people consider trash. My perception of trash has been shifted, there are even more components which can find new life through the hands of maker. The streets of Barcelona are full with materials and furniture. Seeing the people at night, hunting for treasures, is very pleasing to see. Although it is nice that people try to reuse the components. It is only treating the symptoms of our throw-away society.
