---
title: 06. Designing with Extended Intelligence
period: 05 - 11 November 2018
date: 2018-11-11 12:00:00
term: 1
published: true
---
### Visions for Change

Ever since humanity invented an automaton there is this certain uneasy feeling we have. The [Uncanny Valley](https://en.wikipedia.org/wiki/Uncanny_valley) describes this eeriness and strange feeling when we are interacting with an humanoid. Maybe the doubt we have, started back in the antiques. Looking back on ancient stories like Talos, we see that the idea of a robot is not something new. Back then all stories about automaton where from war leaders trying to create the stronger force. But they always failed, still we still pursue these dreams. If we look closer on ancient history we can see in old painting about Talos that there is a tear painted on its/his cheek while he is being strucked 'death'. We feel empathy for it, but still we don't trust *it*. Maybe all this distrust come from the fear that when feed all our *human data* into a artificial intelligence, it will turn out to be like us. Because with trades like care, love and empathy comes also hate, untruthfulness and disgust. There is this timeless link between imagination and science. 
