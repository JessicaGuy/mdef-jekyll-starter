---
title: Biology Zero
period: 08 October - 14 October 2018
date: 2018-10-21 10:00:00
term: 1
published: true
---

## What is Biology
By the webster definition biology is the branch of knowledge which deals with living organisms and vital processes. Basically the Science of life. Reading this sentence makes biology seem like an easy understandable topic. But let me tell you, it is much more then you expect. The past week we had the opportunity to get to know the basics of biology. Núria Conde Pueyo and Jonathan Minchin where generous enough to take the time teaching us. There are nine main branches of biology, we focused manly on biochemistry. Within this topic we explored the state of the art of bio-/ genetic-/ tissue engineering, metabolism, microbiology, cellular biology, histology etc.. As well as why DIY biohacking is very interesting especially for a non-biologist. Furthermore we had the pleasure of three additional talks from Thora H. Arnardottir, Jessica Dias and Mette Bak-Anderson.

![]({{site.baseurl}}/181020_BiologyZero_Icons01.jpg)

## Biology and Design? 
As a designer there is one question which pops into my mind with an instant. How can one implement biology into design? And/ or are we doing it already? Engineering, sociology and programming are one of the few topics with which designers are already being confronted with. Being able to know about e.g. web development helps to understand structures, procedures and enhances the web design. It also helps with the communication with professionals from that specific field of work. Knowing that designers always have been working in a multidisciplinary atmosphere, biology seems like a next logical step. With a curious mind a designer is well suited for implementing scientific topics into the life of an average person. Three great examples for how biology can be implemented into design are Thora H. Arnardottir and Jessica Dias from the Biocentric Design Group and Mette Bak-Anderson from Copenhagen School of Design and Technology.

![]({{site.baseurl}}/181020_BiologyZero_Icons02.jpg)

## The Details / What we have learned so far

### Bioengineering
Since the big bang the world evolved from simple molecules to the complex system of a human. We humans are having a significant impact on earths ecosystem. Because of that, we call the time in which we are living in anthropocene. Another example for our influence on earth is bioengineering. With bioengineering we are using the mindset of an engineer to modify tissue, cells or molecules to our needs. There many examples for that. One of the most controversial ones is probably genetic engineering like CRISP-CAS9. Supporters of Gen-engineering say that after what we have done to earth, it is our responsibility to use our knowledge for the good. We have been gifted with consciousness. We should use that to be an active player in evolution. With bioengineering you can modify e.g. the lifespan and size of animals. There are possibilities to modify pigs to be ‘clean’. With that they won’t provoke rejection when implanting pig organs into humans. Also the risk of inheriting pig specific viruses can be lowered. A lot is possible in science, but what we need is reasonable implementation.

![]({{site.baseurl}}/181020_BiologyZero_Icons03.jpg)

### Metabolism
Through the cellular process of converting food to energy, we are able to be alive. This does not only happen within humans. Every cell on earth follows this procedure. This allows organisms to grow and reproduce. There are two main categories: Anabolism and Catabolism. Anabolism is a bond-making process that forms larger macromolecules. It requires an input of energy (ATP). Catabolism breaks the bond of macromolecules, releasing energy to create ATP. Certain reaction happen on their own, but complex reactions need proteins (enzymes) to be able to join. Enzymes catalyse the chemical reactions. For example: the human catabolism is using aerobic respiration. One could call that the first circular economy ever.

![]({{site.baseurl}}/181020_BiologyZero_Icons04.jpg)

### Microbiology
All living organisms which can be called alive, unicellular or cell-cluster, are studied in the field of Microbiology. Viruses though are not strictly classified as a living organism, because they need their host to stay alive. Nevertheless they are also studied within the field. Organisms are classified in three branches: Nutrition, information management and kingdoms. To isolate microorganism you need to: Take a sample of interest, know about respiration and nutritional, temperature etc., prepare an auxetic medium and strick the sample to the medium. To identify them you need to: Look for the colony shape using microscopic observation and use a microbial sequencing method.

![]({{site.baseurl}}/181020_BiologyZero_Icons05.jpg)

### Cell biology
As mentioned above, there are cells inside of every organism. Cell biology is a field of science in which the structure, physiology and biorhythm of a cell is studied. Mitoses and Meiosis are fundamental reproduction processes of cells. In the eukaryotic cell division process of mitoses the DNA is first replicated. Then the chromosomes split and line up around the equator of the cell. The original cell divides into two identical cells, incorporating two identical sets of the chromosomes. Mitosis is used in almost all of our body’s cell devision. Meiosis in comparison is only used for copying cells in sexual reproduction. In the process of homologous chromosomes trading parts, there is an exchange of the DNA information. The daughter cells then have half as many chromosomes than the original cell.

![]({{site.baseurl}}/181020_BiologyZero_Icons06.jpg)

### Histology
Tissue samples from plants, animals and humans include different kind of cells. Histology is the study about how these cells are arranged. Using a microscope one can examine a slice of tissue. Haematoxylin and eosin are stains to used in histology to enhance the structures, which make them easier to identify.

![]({{site.baseurl}}/181020_BiologyZero_Icons07.jpg)

## DIY Biolab
We had the opportunity to deepen our knowledge about biology with hands-on experience in the biolab. We have started with preparing the individual auxetic medium for the bacteria we wanted to grow. There are two processes to achieve growing only one bacteria: Isolation and cultivation. This means feeding the bacteria which you wanna keep, with specific food, the remaining unwanted bacteria will then starve to death.

The medium which was made
- Vibra Fisheri with lesogeny broth medium (Marine)
- Sacaharomyces cerevisiae with yeast extract-peptone-dextrose medium (YPD)
- LactoBacillos with man-rogosar-sharpe medium (MRS - peptonized milk with tomato juice)
- Lucia Bertani (LB - Generic media for bacteria)

The process of making
- Make medium with the recipe
- Sterilise the glasses with medium in pressure cooker (leave the glasses open slightly so they wont burst while being heated) with a piece of cloth on the bottom
- It will be cooked in the steam not in the water/ Steam is hotter that 100 degrees
leave medium in cooker for at least 15 min (you can tell the time by the autoclave tape)
- Clean space and label petri dishes
- Fill petri dishes with medium and let cool
- Seed or inoculate petri dishes (label them)
- Put them upside down in the incubator (not the marine one). They are kept upside down so the condensation water stays on the lid.

After preparing the medium and seeding the petri dish, we let the bacteria grow for 48 hours. In another Experiment we used stoichiometry for a chemical quantification, to find out how much calcium carbonate there is in 5g of eggshell. Also we were shown the procedure of a spectrophotometry. In the second Experiment we learned how to use a mini Polymerase chain reaction (PCR) to stimulate DNA to replicate.

![]({{site.baseurl}}/181020_BiologyZero_Icons08.jpg)

## Speculative Project
Each one of our group had the task to reassess known structures, designs and ideas. And to think about possible fusions between these topics and biology, based on the knowledge we have gained during that week. I am interested in how Bacteria could inform us about environmental changes. For example is algae is used as an indicator for water. And lichens are a biological indicator for air quality. The interesting essence of this topic is that is how bacterial knowledge can be translated or transferred through visualisation which can be understood by any human?

An interspecies dialogue can be realised or created by a common syntax shared by bacteria and humans.

Methodology
- Analyze human communication(techniques)
- Analyze bacteria communication (processes)
- Examine the essence of communication of humans and bacteria
- Evaluate the underlying structure and formulate the correlating syntax
- Test the conclusion of syntax (if necessary reevaluate and restructure)

## Reflection
The Biology Zero week was intense and amazing. It provided us with an insight of the basics of biology and biohacking. I think it was a great start for those who see a potential in biodesign. Personally I am not sure if I can see my future in biodesign, it defiantly has awaken my old love to bio-material research.
