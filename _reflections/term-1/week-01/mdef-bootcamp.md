---
title: MDEF Bootcamp
period: 01 - 07 October 2018
date: 2018-10-07 12:00:00
term: 1
published: true
---

### Gitlab
This week was all about getting started. After getting a brief introduction from Tomas and Oscar, we have tackled our **first big thing**, Gitlab. I think Gitlab is for the most of us something new. Not knowing about coding or Web development this was quite a bit to handle. I designed Websites before, but I have yet not had the pleasure of writing HTML or similar. Although I have to say with the help of the tutors and fellow students it wasn’t a bad start. Coding opens a new door for me. I am very curious what I can achieve with it.

### City Safari and Inventory
We had the great pleasure of getting a tour through Poblenou. Barcelona seems to have hidden gems spread all over the city. During the mornings all the garages and doors are closed. Nothing special appears to be behind the sometimes run down facades. But behind these old sprayed doors there multiple great stores, workshops, communities and companies. Indissoluble - The exhibition factory is one of these great places.

![]({{site.baseurl}}/181007_Week1_Photos01.jpg)

Indissoluble is a space near our University. They design spaces for events and exhibitions. Within their walls they design and build their ideas. It is impressive how well this works out. Transfolab is another example for interesting maker spaces close to IAAC.

![]({{site.baseurl}}/181007_Week1_Photos02.jpg)

Transfolab opens their doors for all the makers, inventors and creative people who want to reuse or produce goods. They offer a great material library with scraps and bits found on the streets of Poblenou. Also there is an office area to generate ideas and a workshop to realize them. Just around the corner of IAAC is Biciclot, a lovely bike repair shop and educational community.

![]({{site.baseurl}}/181007_Week1_Photos03.jpg)

They not only offer secondhand bicycles and a bike repair. But also workshops for locals to actually learn themselves how to repair their bikes. Also they invite people with no, or almost no educational background, to be a trainee within their bike-shop to become a bicycle mechanic. This gives the people the opportunity to reevaluate their job chances.

After the get-together with our neighbours we were sent off to our own scavenger hunt. We all have had major fun collecting materials from our neighbourhood. It was very interesting to see what kind of materials emerge from our area. There different household articles but also a lot of construction materials which we were able to pick up. During the last day of the week we made an inventory on what kind of communities are within the realms of Poblenou. It was very interesting to see how rich this district is. Not in a sense of how much it is financially worth. But in a sense of how many different interesting people come together. There are so many projects rising from here and there are a lot of opportunities to create something interesting.

### Learning process
It is not so easy to explain how I have learned what I have learned. There is a sender and receiver model of communication. In that case you would have the teacher who is sending information and me who is receiving it. But in our case it is much more complex than that. There is knowledge exchange between all the students and the environment. But in generally speaking my process of learning could be broken down to these key topics.

- Opening yourself/ not being prejudice
- Curiosity/ Seeking for something new / Seeing the world like a kid
- Investigating/ exploring what was found
- Being Active - Tactile interaction
- Teamwork - Knowledge exchange

### What now?
Within one week my horizon was already broaden. I knew that being in such a inspiring community it can only bring you further. This past week was a perfect example for it. We helped each other out where ever it was possible, I really enjoy this feeling. Through this input there are many questions rising and I am excited to find some answers.

- How can we translate what we are doing?
- Why is there a gap between the makers and the residents in Poblenou?
- And what can we do about that?
- Where are spaces for interventions?
- Why is the super block empty and not green?
- What do we (the residents) want and need in our neighborhood?
- Without the conflicts which are already rising?
- What can we make, what residents want?
- Why is the city park able to have a reliable space but urban gardeners not?
- How can we make the city more livable? Maybe more green?
- Why are there so many empty rooftops in Barcelona?
- Why is the recycling so bad in Barcelona?
- Or why is it so badly explained to newcomers?
- What is happening in the community centers?
- Who uses the community centers? For what?
- Is there a need for more community centers?
- Or do we have to enhance the existing one?
- Is there a specified plastic recycling centre in Barcelona?
- If not is there a need for one?
- How active is the precious plastic community in Barcelona?
- How can we optimize sustainable thinking?
- How can we implement sustainable ways of living within communities?
- How and where do we move through the city?
- Can these paths be optimized?
- Why doesn't google maps know where the bike roads are?
