---
title: Navigating the Uncertainty
period: 29 November - 04 November 2018
date: 2018-11-07 12:00:00
term: 1
published: true
---

This week was a rollercoaster. We have started of with two talks from Jose Luis Vincente and Pau Alsina. They both were very interesting and inspiring. Unfortunately Indy Johar was not able to come, he would have been a great uplifting end for this session. But Marianna and Tomas filled in, we were able to discuss and reflect what we have learned so far.

The first talk was full of information, I had troubles receiving, processing all of it and then writing down what I've heard. I am trying to enhance my memory through actively remembering, instead of writing down every single word. But I do have to say that writing down what I've heard, to revise it later and then write about it here, is more reliable. Jose talked about his work as a currator. He told us before, that we are going to know a lot of the facts. But it is sometimes good to remind ourselves again. Sometimes we tend to forget was is happening outside in the world. Especially if we are currently not effected by it. This reminded me again why we are here. We are here to make a change. To find interventions, a starting point for rethinking systems. We might not know where the path is leading us at the moment. But so far it has been very interesting. He encouraged us to rethink 'What is now' and the *scale of now*. The *Anthropocene* is this little part of earths history. Which officially started on the 16th of July in 1945. That was the day on which geologists say that we have had a significant impact on earth, which can be seen by future generations. Just as we today investigate the soil and the ice to analyze the past. We are constantly trying to change, modify and optimize the earth to our desires. Sometimes we even forget that if we used up all the resources earth has to offer, we are going to be to one extinct, not earth. We live in a time of *constant acceleration*. We have to use this time to *modify and optimize ourselves*. To create better manufacturing processes, a not corrupt government and a society in which we work together, not against each other. I can defiantly see myself as an optimist or opportunist, just as Tomas said. *And maybe we are able to change without a trauma, a change for the good.*

![]({{site.baseurl}}/181108_Sketch_01.jpg)

The second talk was from Pau Alsina. Pau made us rethink the role of design in our society. When you start rethinking what a table is you see all the meanings it could have. A table is called table because we invented the terms and notions. An 'alien' would not understand what we are talking about and would not have the same understanding of its impact. Everyone has a different views on *what design is*, depending on their cultural background, biological and personal condition. A great example for this perception is the movie 'Arrival'. Anyone who has not seen yet, watch it now! I rewatched it the same day, its still great.

The same evening we went to a talk from [All Women](https://www.allwomen.tech) a society for all female experts on technology and Artificial Intelligence (AI). It was interesting to hear good and bad examples of where AI is already being used. The Data which AI uses is very sensitive. Depending on which language you use to teach the algorithm things can go bad, really quick. And even though we think AI should not be biased. It is not. Because everything we feed into technology comes from us. Ultimatly feeding the machine with biased information, even if we don't intend to. Which does not have to be bad. Because in the end all data is biased. Because how we look at things will always be a point of view. But we always have to choose our words for the algorithm wisely. Because *technology is not bad, we just have to learn how to use it*. There are two questions rising from this talk. *And how can we design trust when using data and AI?* *How can we use AI responsible and make the process more transparent?*

![]({{site.baseurl}}/181108_Sketch_2.jpg)

Marianas presentation informed us about that everything on earth breaks down to *exchanging information* based on read-write technologies. Within flora and fauna, humans and all physical systems. How we are sending, receiving and processing it is depending of our point of view. I would love to able to expand my mind to understand the world like a tree network. The parallel universe in which we evolved based on a different main sense than the visual, would be interesting to see. How would we look like if our main sense would have been smelling or hearing? Or based on a sense which we don't even know of? At least we were able to invent different readers to understand the world. But I think there is much more we don't understand yet, more to receive and process then we can imagine at the moment.

![]({{site.baseurl}}/181108_Sketch_3.jpg)

### Raising Questions
- What is the core Idea of moral judgement?
- How can we measure the impact of design?
- What is consciousness and when does it emerge?
- When does it become manifested within AI to cognitively assert to 'I am'?
- Are we able to think outside of the box if we search for specific things based on our perception?
- Why do we have the idea that we have to have the most of something?
- How would our world look like today if minimalism would have been the root of our society?
- Are we able to combine our forces to become *one* federation?
- How can focus on exploring instead of conquering?
- Why is this uneasy feeling rooted deep within humanity against *the other*?
- How can we create a transparent environment around AI?
- How can we design trust in Data and AI?

### To read

[The Ghost in the Machine](https://www.goodreads.com/book/show/30677.The_Ghost_in_the_Machine) by Arthur Koestler
"Koestler examines the notion that the parts of the human brain-structure which account for reason and emotion are not fully coordinated. This kind of deficiency may explain the paranoia, violence, and insanity that are central parts of human history, according to Koestler's challenging analysis of the human predicament."

[Gut Feelings: The intelligence of the unconscious](https://www.goodreads.com/book/show/786560.Gut_Feelings) by Gert Gigerenzner
"...Gigerenzer explains why our intuition is such a powerful decision-making tool. Drawing on a decade of research at the Max Plank Institute, Gigerenzer demonstrates that our gut feelings are actually the result of unconscious mental processes—processes that apply rules of thumb that we’ve derived from our environment and prior experiences. The value of these unconscious rules lies precisely in their difference from rational analysis—they take into account only the most useful bits of information rather than attempting to evaluate all possible factors."

[Can we design trust between humans and artificial intelligence](https://www.fastcompany.com/3047500/can-we-design-trust-between-humans-and-artificial-intelligence)

[In the age of AI is seeing still believing?](https://www.newyorker.com/magazine/2018/11/12/in-the-age-of-ai-is-seeing-still-believing)

[Resisting Reduction: A Manifesto/ Designing our Complex Future with Machines.](https://jods.mitpress.mit.edu/pub/resisting-reduction)

[Design Justice, A.I., and Escape from the Matrix of Domination](https://jods.mitpress.mit.edu/pub/costanza-chock)

[Myth and the Making of AI](https://jods.mitpress.mit.edu/pub/holmes-mccue)

[Data trust by design principles patterns and best particles](https://medium.com/greater-than-experience-design/data-trust-by-design-principles-patterns-and-best-practices-part-1-defffaac014b)

[How can we design AI that we trust? ](https://www.youtube.com/watch?v=aL0JUUMvQSA)(to watch)

[AI Mankinds last Invention](https://www.youtube.com/watch?v=Pls_q2aQzHg) (to watch)

[Can we build AI without losing control over it?](https://www.youtube.com/watch?v=8nt3edWLgIg) (to watch)
